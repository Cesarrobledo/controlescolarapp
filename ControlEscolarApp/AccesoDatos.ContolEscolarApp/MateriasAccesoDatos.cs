﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConexionBd;
using Entidades.ControlEscolarApp;
using System.Data;

namespace AccesoDatos.ContolEscolarApp
{
    public class MateriasAccesoDatos
    {
        Conexion _conexion;

        public MateriasAccesoDatos()
        {
            _conexion = new Conexion("localhost", "root", "", "escolar", 3306);
        }

        public void Eliminar(int idMateria)
        {
            string cadena = string.Format("Delete from Materia where idMateria = {0} ", idMateria);
            _conexion.EjecutarConsulta(cadena);
        }

        public void Guardar(Materia materia)
        {
            if (materia.IdMateria == "")
            {              
                string cadena = string.Format("insert into Materia values(null, '{0}', {1}, {2})", materia.NombreMateria, materia.MateriaAnterior, materia.MateriaSiguiente);
                _conexion.EjecutarConsulta(cadena);
            }
            else
            {
                string cadena = string.Format("Update Materia set nombreMateria = '{0}', materiaAnt = {1}, MateriaSig = {2} where idMateria = {3}", materia.NombreMateria, materia.MateriaAnterior, materia.MateriaSiguiente, materia.IdMateria);
                _conexion.EjecutarConsulta(cadena);
            }
        }
        public DataSet ObtenerDatos(string filtro)
        {
            string consulta = string.Format("SELECT p.idMateria ID, p.nombreMateria Nombre, a.nombreMateria Anterior, s.nombreMateria Siguiente "+
                "FROM Materia p LEFT JOIN Materia a "+
                "ON p.materiaAnt = a.idMateria "+
                "LEFT JOIN Materia s "+
                "ON p.materiaSig = s.idMateria WHERE p.nombreMateria LIKE '%{0}%'", filtro);
            return _conexion.ObtenerDatos(consulta, "Materia");
        }
    }
}
