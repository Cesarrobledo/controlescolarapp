﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolarApp
{
    public class AlumnosInscritos
    {
        private string _idGrupo;
        private string _noControl;

        public string IdGrupo { get => _idGrupo; set => _idGrupo = value; }
        public string NoControl { get => _noControl; set => _noControl = value; }
    }
}
