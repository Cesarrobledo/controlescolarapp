﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolarApp
{
    public class Grupo
    {
        private string _id;
        private string _nombre;
        private string _carrera;
        public string Id { get => _id; set => _id = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string Carrera { get => _carrera; set => _carrera = value; }
    }
}
