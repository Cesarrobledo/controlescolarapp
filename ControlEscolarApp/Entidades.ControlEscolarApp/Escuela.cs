﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolarApp
{
    public class Escuela
    {
        private int _idescuela;
        private string _nombre;
        private string _director;
        //private string _domicilio;
        //private string _numeroexterior;
        //private string _estado;
        //private string _municipio;
        //private int _telefono;
        //private string _e_mail;
        //private string _paginaweb;
        private string _logo;

        public int Idescuela { get => _idescuela; set => _idescuela = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string Director { get => _director; set => _director = value; }
        //public string Domicilio { get => _domicilio; set => _domicilio = value; }
        //public string Numeroexterior { get => _numeroexterior; set => _numeroexterior = value; }
        //public string Estado { get => _estado; set => _estado = value; }
        //public string Municipio { get => _municipio; set => _municipio = value; }
        //public int Telefono { get => _telefono; set => _telefono = value; }
        //public string E_mail { get => _e_mail; set => _e_mail = value; }
        //public string Paginaweb { get => _paginaweb; set => _paginaweb = value; }
        public string Logo { get => _logo; set => _logo = value; }
    }
}
