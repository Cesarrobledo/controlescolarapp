﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolarApp;
using AccesoDatos.ContolEscolarApp;
using System.Data;
using System.Text.RegularExpressions;

namespace LogicaNegocio.ControlEscolarApp
{
    
    public class MateriasManejador
    {
        private MateriasAccesoDatos _materiaAccesoDatos;

        public MateriasManejador()
        {
            _materiaAccesoDatos = new MateriasAccesoDatos();
        }

        public void Eliminar(int idMateria)
        {
            _materiaAccesoDatos.Eliminar(idMateria);
        }

        public void Guardar(Materia materia)
        {
            _materiaAccesoDatos.Guardar(materia);
        }

        public DataSet ObtenerDatos(string filtro)
        {
            return _materiaAccesoDatos.ObtenerDatos(filtro);
        }

        private bool NombreValido(string Nombre)
        {
            var regex = new Regex(@"^[A-Za-z]+( [A-Za-z]+)*$");
            var match = regex.Match(Nombre);

            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public Tuple<bool, string> EsNombreValido(Materia materia)
        {
            string mensaje = "";
            bool valido = true;

            if (materia.NombreMateria.Length == 0)
            {
                mensaje = "El nombre de la materia es necesario";
                valido = false;
            }
            else if (!NombreValido(materia.NombreMateria))
            {
                mensaje = "Escribe un fomato valido para el nombre";
                valido = false;
            }
            else if (materia.NombreMateria.Length > 50)
            {
                mensaje = "La longitud para nombre de la materia es máximo 50 caracteres";
                valido = false;
            }
            return Tuple.Create(valido, mensaje);
        }
    }
}
