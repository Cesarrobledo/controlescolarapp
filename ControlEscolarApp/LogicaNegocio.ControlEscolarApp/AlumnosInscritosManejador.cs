﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatos.ContolEscolarApp;
using System.Data;
using Entidades.ControlEscolarApp;

namespace LogicaNegocio.ControlEscolarApp
{
    public class AlumnosInscritosManejador
    {
        AlumnosInscritosAccesoDatos _alumnosInscritosAccesoDatos;

        public AlumnosInscritosManejador()
        {
            _alumnosInscritosAccesoDatos = new AlumnosInscritosAccesoDatos();
        }

        public DataSet ObtenerAlumnosGrupo(string filtro, string filtro2)
        {
            return _alumnosInscritosAccesoDatos.ObtenerAlumnosGrupo(filtro, filtro2);
        }

        public bool VerificarInscripcion(string noControl, DataSet lista)
        {
            bool existe = false;
            for (int i = 0; i < lista.Tables[0].Rows.Count; i++)
            {
                if (lista.Tables[0].Rows[i][0].ToString() == noControl)
                    existe = true;
            }
            return existe;
        }

        public bool Guardar(AlumnosInscritos alumnoInscrito)
        {
            return _alumnosInscritosAccesoDatos.Guardar(alumnoInscrito);
        }
        public bool Eliminar(string noControl)
        {
            return _alumnosInscritosAccesoDatos.Eliminar(noControl);
        }
    }
}
