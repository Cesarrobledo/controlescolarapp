﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolarApp;
using LogicaNegocio.ControlEscolarApp;


namespace ControlEscolarApp
{
    public partial class PrincipalGrupos : Form
    {
        GruposManejador _gruposManejador;
        Grupo _grupo;
        public PrincipalGrupos()
        {
            InitializeComponent();
            _gruposManejador = new GruposManejador();
            _grupo = new Grupo();
        }

        private void PrincipalGrupos_Load(object sender, EventArgs e)
        {
            BuscarGrupo("");
        }
        private void BuscarGrupo(string filtro)
        {
            Visor.DataSource = _gruposManejador.ObtenerDatos(filtro).Tables[0];
            Visor.AutoResizeColumns();
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarGrupo(txtBuscar.Text);
        }

        private void btnEliminarGrupo_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar esta materia", "Eliminar Materia", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                try
                {
                    EliminarGrupo();
                    BuscarGrupo("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void EliminarGrupo()
        {
            int idGrupo = int.Parse(Visor.CurrentRow.Cells[0].Value.ToString());
            _gruposManejador.Eliminar(idGrupo);
        }

        private void btnNuevoGrupo_Click(object sender, EventArgs e)
        {
            GruposModal gruposModal = new GruposModal(null);
            gruposModal.ShowDialog();
            BuscarGrupo("");
        }

        private void Visor_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            BuildEscuela();
            GruposModal gruposModal = new GruposModal(_grupo);
            gruposModal.ShowDialog();
            BuscarGrupo("");
        }

        private void BuildEscuela()
        {
            _grupo.Id = Visor.CurrentRow.Cells[0].Value.ToString();
            _grupo.Nombre = Visor.CurrentRow.Cells[1].Value.ToString();
            _grupo.Carrera = Visor.CurrentRow.Cells[2].Value.ToString();
        }
    }
}
