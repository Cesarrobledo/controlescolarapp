﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolarApp;
using LogicaNegocio.ControlEscolarApp;

namespace ControlEscolarApp
{
    public partial class InscripcionModal : Form
    {
        AlumnosInscritos _alumnoInscripcion;
        private AlumnosManejador _alumnosManejador;
        private AlumnosInscritosManejador _alumnosInscritosManejador;
        private Grupo _grupo;

        public InscripcionModal(Grupo grupo)
        {
            InitializeComponent();
            _alumnoInscripcion = new AlumnosInscritos();
            _alumnosManejador = new AlumnosManejador();
            _alumnosInscritosManejador = new AlumnosInscritosManejador();
            _grupo = new Grupo();
            _grupo = grupo;
            ListGroupLoad("");
        }

        private void BuildInscripcionAlumno()
        {
            _alumnoInscripcion.IdGrupo = _grupo.Id;
            _alumnoInscripcion.NoControl = VisorAlumnos.CurrentRow.Cells[0].Value.ToString();
        }

        private void InscripcionModal_Load(object sender, EventArgs e)
        {
            AlumnosLoad("");
            lblNombreGrupo.Text = "Grupo: " + _grupo.Nombre + "     Carrera: "+ _grupo.Carrera;
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            BuildInscripcionAlumno();
            if (!VerificarInscripcion())
                _alumnosInscritosManejador.Guardar(_alumnoInscripcion);
            else
                MessageBox.Show("Este alumno ya esta registrado en este grupo", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            ListGroupLoad("");
        }

        private bool VerificarInscripcion()
        {
            var noControl = _alumnoInscripcion.NoControl;
            var lista = _alumnosInscritosManejador.ObtenerAlumnosGrupo(_grupo.Id, "");

            return _alumnosInscritosManejador.VerificarInscripcion(noControl, lista);
        }

        private void btnQuitar_Click(object sender, EventArgs e)
        {
            var noControl = VisorLista.CurrentRow.Cells[0].Value.ToString();
            _alumnosInscritosManejador.Eliminar(noControl);
            ListGroupLoad("");
        }

        private void ListGroupLoad(string filtro)
        {
            VisorLista.DataSource = _alumnosInscritosManejador.ObtenerAlumnosGrupo(_grupo.Id, filtro).Tables[0];
        }

        private void AlumnosLoad(string filtro)
        {
            VisorAlumnos.DataSource = _alumnosManejador.ObtenerLista(filtro);
        }

        private void txtBuscarAlumno_TextChanged(object sender, EventArgs e)
        {
            AlumnosLoad(txtBuscarAlumno.Text);
        }

        private void txtBuscarAlumnoGrupo_TextChanged(object sender, EventArgs e)
        {
            ListGroupLoad(txtBuscarAlumnoGrupo.Text);
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
